#!/bin/sh


##########################################################

# EFISIZE = +550M
# SWAPSIZE = RAM + 2G

##########################################################
#                          MANUAL                        #
##########################################################

# loadkeys colemak
# setfont ter-124b
# ping google.com

# sfdisk --delete /dev/sda

# fdisk /dev/sda
# g
# n
# ENTER
# ENTER
# EFISIZE
# n
# ENTER
# ENTER
# SWAPSIZE
# n
# ENTER
# ENTER
# ENTER
# t
# 1
# 1
# t
# 2
# 19
# t
# 3
# 20
# w

# mkfs.fat -F32 /dev/sda1
# mkswap /dev/sda2
# swapon
# mkfs.btrfs /dev/sda3

# archinstall
# additional packages: git neovim linux-headers

# arch-chroot
# cd /tmp
# git clone https://gitlab.com/IwIg0/arch.git install
# cd install
# ./install.sh

##########################################################
#                        AUTOMATIC                       #
##########################################################

sed -i s/#en_US.UTF-8/en_US.UTF-8/ /etc/locale.gen
sed -i s/#fr_FR.UTF-8/fr_FR.UTF-8/ /etc/locale.gen

locale-gen

cp ./keyboard/colemak-alt.map.gz /usr/share/kbd/keymaps/i386/colemak/
cp ./keyboard/us /usr/share/X11/xkb/symbols/
cp ./keyboard/base.lst /usr/share/X11/xkb/rules/
cp ./keyboard/base.xml /usr/share/X11/xkb/rules/
cp ./keyboard/evdev.lst /usr/share/X11/xkb/rules/
cp ./keyboard/evdev.xml /usr/share/X11/xkb/rules/

cp ./fonts/* /usr/share/kbd/consolefonts/

printf "
FONT=ter-124b
KEYMAP=colemak-alt
" > /etc/vconsole.conf

printf "\n"
printf "Drivers amd: amdgpu | intel: i915\n"
printf "Add <driver> first in MODULES\n"
printf "Add 'consolefont' after 'keymap' in HOOKS\n"
printf "Press [ENTER]"
read -r _

nvim /etc/mkinitcpio.conf
mkinitcpio -p linux
